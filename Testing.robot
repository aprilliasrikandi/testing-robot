*** Settings ***
Library           Selenium2Library
Library           OperatingSystem

*** Variables ***
${Browser}        Firefox
${SiteUrl}        https://twitter.com/signup

*** Test Cases ***
Twitter Registration Flow
    Open page

*** Keywords ***
Open page
    open browser    ${SiteUrl}    ${Browser}
    Maximize Browser Window
